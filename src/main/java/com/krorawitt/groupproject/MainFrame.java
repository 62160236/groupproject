/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.krorawitt.groupproject;

/**
 *
 * @author WINDOWS 10
 */
public class MainFrame extends javax.swing.JFrame {

    /**
     * Creates new form Test
     */
    public MainFrame() {
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        scrnumber2 = new javax.swing.JScrollPane();
        scrnumber1 = new javax.swing.JScrollPane();
        btnKrorawit = new javax.swing.JButton();
        btnCharantorn = new javax.swing.JButton();
        btnSuwijak = new javax.swing.JButton();
        btnJRTW = new javax.swing.JButton();
        scrnumber3 = new javax.swing.JScrollPane();
        btnNuttapon = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        btnKrorawit.setText("Krorawit");
        btnKrorawit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnKrorawitActionPerformed(evt);
            }
        });
        scrnumber1.setViewportView(btnKrorawit);

        btnCharantorn.setText("Charantorn");
        btnCharantorn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCharantornActionPerformed(evt);
            }
        });

        btnSuwijak.setText("Suwijak");
        btnSuwijak.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSuwijakActionPerformed(evt);
            }
        });

        btnJRTW.setText("Jaturawit");
        btnJRTW.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnJRTWActionPerformed(evt);
            }
        });

        btnNuttapon.setText("Nuttapon");
        btnNuttapon.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNuttaponActionPerformed(evt);
            }
        });
        scrnumber3.setViewportView(btnNuttapon);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(19, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(btnCharantorn, javax.swing.GroupLayout.PREFERRED_SIZE, 188, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(scrnumber1, javax.swing.GroupLayout.PREFERRED_SIZE, 188, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnSuwijak, javax.swing.GroupLayout.PREFERRED_SIZE, 188, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnJRTW, javax.swing.GroupLayout.PREFERRED_SIZE, 188, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(scrnumber3, javax.swing.GroupLayout.PREFERRED_SIZE, 188, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(scrnumber2, javax.swing.GroupLayout.PREFERRED_SIZE, 656, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(scrnumber2)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(scrnumber1, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(26, 26, 26)
                .addComponent(scrnumber3, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(34, 34, 34)
                .addComponent(btnSuwijak, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(36, 36, 36)
                .addComponent(btnJRTW, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 116, Short.MAX_VALUE)
                .addComponent(btnCharantorn, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(120, 120, 120))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnJRTWActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnJRTWActionPerformed
        scrnumber2.setViewportView(new JTRWPanel());
    }//GEN-LAST:event_btnJRTWActionPerformed

    private void btnNuttaponActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNuttaponActionPerformed
        scrnumber2.setViewportView(new MaxPanel());
    }//GEN-LAST:event_btnNuttaponActionPerformed

    private void btnKrorawitActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_btnKrorawitActionPerformed
        scrnumber2.setViewportView(new PhaPanel());
    }// GEN-LAST:event_btnKrorawitActionPerformed

    private void btnCharantornActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_btnCharantornActionPerformed
        scrnumber2.setViewportView(new PurePanel());
    }// GEN-LAST:event_btnCharantornActionPerformed

    private void btnSuwijakActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_btnSuwijakActionPerformed
        scrnumber2.setViewportView(new AomsinPanel());
    }// GEN-LAST:event_btnSuwijakActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        // <editor-fold defaultstate="collapsed" desc=" Look and feel setting code
        // (optional) ">
        /*
         * If Nimbus (introduced in Java SE 6) is not available, stay with the default
         * look and feel. For details see
         * http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        // </editor-fold>
        // </editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new MainFrame().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCharantorn;
    private javax.swing.JButton btnJRTW;
    private javax.swing.JButton btnKrorawit;
    private javax.swing.JButton btnNuttapon;
    private javax.swing.JButton btnSuwijak;
    private javax.swing.JScrollPane scrnumber1;
    private javax.swing.JScrollPane scrnumber2;
    private javax.swing.JScrollPane scrnumber3;
    // End of variables declaration//GEN-END:variables
}
